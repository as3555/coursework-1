#ifndef _EVENTS_H_
#define _EVENTS_H_

#include <string>
#include <iostream>

class Event
{
    //Private member variables
    private:
    std:: string name;

    //Public member functions
    public:
    Event(std::string name);
    void allEvents();
    virtual ~Event();
};

#endif
